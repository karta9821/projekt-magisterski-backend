import platform
from invoke import task
from pathlib import Path


SERVICE_NAME = "app"
WINDOWS_PLATFORM = platform.system().lower() == "windows"
PTY = False if WINDOWS_PLATFORM else True


@task
def clear_cache(context):
    context.run(" ".join([
        r'find . | grep -E "(__pycache__|\.pyc|\.pyo$)" |'
        f"{'' if WINDOWS_PLATFORM else 'sudo '}xargs rm -rf"
    ]))


@task
def prepare_docker_network(context):
    context.run("docker network create msc")


@task
def prepare_postgres_container(context):
    context.run(
        " ".join([
            "docker run -d --name=msc_psql",
            "--network=msc --restart=unless-stopped",
            "-p 5432:5432",
            "-e POSTGRES_USER=msc",
            "-e POSTGRES_PASSWORD=msc",
            "-e POSTGRES_DB=msc",
            "postgres:12-alpine",
        ])
    )


@task
def prepare_redis_container(context):
    context.run(
        " ".join([
            "docker run -d --name=msc_redis",
            "--network=msc --restart=unless-stopped",
            "-p 6379:6379",
            "redis:6-alpine",
            "--requirepass msc",
        ])
    )


@task
def prepare_flower_container(context):
    context.run(
        " ".join([
            "docker run -d --name=msc_celery_flower",
            "--network=msc --restart=unless-stopped",
            "--env-file .env",
            "-p 8889:8888",
            "mher/flower",
        ])
    )


def get_image_name(tag="latest"):
    return f"{Path(__file__).parent.name}_{SERVICE_NAME}:{tag}"


@task
def build(context, no_cache=None):
    context.run(
        " ".join(
            [
                "docker",
                "build",
                "--no-cache" if no_cache else "",
                "-t",
                f"{get_image_name()}",
                ".",
            ]
        ),
        pty=PTY,
    )


@task
def image_rm(context):
    context.run(f"docker image rm {get_image_name()}")


@task
def make_migration(context, name, autogenerate=False):
    context.run(
        " ".join([
            "docker-compose run",
            SERVICE_NAME,
            "alembic revision",
            "--autogenerate" if autogenerate else "",
            f'-m "{name}"',
        ])
    )


@task
def update_database(context):
    context.run(
        f"docker-compose run {SERVICE_NAME} alembic upgrade head"
    )


@task
def style(context, hide=False):
    context.run(
        " ".join(
            [
                "docker-compose",
                "run",
                "--rm",
                "--no-deps",
                "--entrypoint=black",
                SERVICE_NAME,
                ".",
            ]
        ),
        pty=PTY,
        hide=hide,
    )
    context.run(
        " ".join(
            [
                "docker-compose",
                "run",
                "--rm",
                "--no-deps",
                "--entrypoint=isort",
                SERVICE_NAME,
                ".",
            ]
        ),
        pty=PTY,
        hide=hide,
    )


@task
def lint(context, count=False, hide=False):
    context.run(
        " ".join(
            [
                "docker-compose",
                "run",
                "--rm",
                "--no-deps",
                "--entrypoint=flake8",
                SERVICE_NAME,
                ".",
                "--count" if count else "",
            ]
        ),
        pty=PTY,
        hide=hide,
    )


@task
def test(context):
    context.run(
        f"docker-compose run --rm {SERVICE_NAME} pytest -s",
        pty=PTY
    )


@task
def coverage(context, keepdb=False):
    """
    Executes a unit test run in coverage mode which gathers code coverage
    data for later report.
    """
    context.run(
        " ".join(
            [
                "docker-compose run --rm",
                SERVICE_NAME,
                "coverage run -m pytest"
            ]
        ),
        pty=PTY
    )


@task
def report(context, show_missing=False):
    """
    Generates a text report from data gathered by the `coverage` command.
    """
    context.run(
        " ".join(
            [
                "docker-compose run --rm --no-deps",
                "--entrypoint=coverage",
                SERVICE_NAME,
                "report",
                "--show-missing" if show_missing else "",
            ]
        ),
        pty=PTY
    )


@task(aliases=["pr"])
def pull_request(context):
    green = "\033[32m"
    white = "\033[37m"

    print("```")
    style(context, hide=True)
    print(f"{green}Style: Done")

    lint(context, hide=True)
    print(f"Linting: Done{white}")

    coverage(context)
    report(context)
    print("```")


@task
def create_bucket(context, name="msc"):
    context.run(
        " ".join(
            [
                "docker-compose",
                "run",
                "--rm",
                "--no-deps",
                "--entrypoint=awsv2",
                SERVICE_NAME,
                "s3api create-bucket",
                f"--bucket {name}",
                "--endpoint-url=http://minio:9000",
            ]
        )
    )
