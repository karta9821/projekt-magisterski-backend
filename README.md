[![pipeline status](https://gitlab.com/karta9821/projekt-magisterski-backend/badges/main/pipeline.svg)](https://gitlab.com/karta9821/projekt-magisterski-backend/commits/main)
[![coverage report](https://gitlab.com/karta9821/projekt-magisterski-backend/badges/main/coverage.svg)](https://gitlab.com/karta9821/projekt-magisterski-backend/commits/main)
# Projekt Magisterski - Backend

## Technologies
- `pip`
- `poetry` for dependency management. Dependencies are listed in `pyproject.toml`
- `FastAPI` as our web framework
- `uvicorn` as our ASGI server
- `nginx` as our Web server
- `SQLAlchemy` as our ORM (when database storage required)
- `Alembic` as our migration generator and runner (when database storage required)
- `Postgres` for database service (when database storage required)
- `celery` for async and periodic tasks (as required)


## Pre-commit
Make sure you have installed [pre-commit](https://pypi.org/project/pre-commit/).

About [pre-commit](https://pre-commit.com/).

Run the command

`pre-commit install`

## Alembic

Alembic tracks changes in models in the  `app/db` module. The Alembic "migration environment" can be found in the
`app/alembic` folder. The `versions` folder holds a list of all migrations for the service. The `env.py` file
is run anytime the migration tool is invoked.

Alembic tracks changes to any models that are imported within the `app/db/base.py` file.

#### Generating a Migration

`alembic revision --autogenerate -m migration_name` if you have alembic installed locally

`inv make-migration` if you want to use a docker container (more in [Invoke tasks](#invoke-tasks))

#### Applying a Migration

`alembic upgrade head` if you have alembic installed locally

`inv update-database` if you want to use a docker container

More information about Alembic can be found [here](https://alembic.sqlalchemy.org/en/latest/index.html).


## Running the Service

In the first step, verify that the docker and docker-compose are working correctly.

Before running the service locally, copy the `.env-template` file and rename it `.env`. The environment
variables for the service will reside here.

The service comes with both Nginx and Uvicorn running in containers. If there is a requirement to add more,
these can be added in the future.

It is recommended to create virtualenv and install poetry, after that install all dependencies: `poetry install`.
One of the dependencies is invoke, which we use as a supporting tool (`inv`).

If this is the first run, prepare:

- network `inv prepare-docker-network`
- redis `inv prepare-redis-container`
- database `inv prepare-postgres-container`

Now we can build application:

`inv build`

and run:

`docker-compose up`

The service will now be running on:

`localhost:8000`

## Celery
Make sure the Redis container is running.

If the Redis container is not running, run this command:

`inv prepare-redis-container`

Celery is a task queue/job queue based on distributed message passing. It is focused on real-time operation, but supports scheduling as well.


Celery is initialized in `app.tasks.celery.py`. New tasks created outside the aforementioned file must be imported by adding it to `celery.conf.imports` as the full path for example

`["app.tasks.file_name"]`.

More about celery [here](https://docs.celeryproject.org/en/stable/).


We can create scheduled tasks through celery beat. How to do it we can find in example [here](https://docs.celeryproject.org/en/stable/userguide/periodic-tasks.html).


If we want to see what is going on with our task, we should go to

`localhost:8888`

and provide login credentials from the .env file.


## API Docs/OpenAPI

When the service is running you can visit the API documentation by visiting:

`localhost:8000/docs`

The project uses automatic generation of documentation from FastAPI (Swagger UI).
Therefore, we make sure that all endpoints are properly described: 
e.g. expected body structure, properties, headers, return http code, ...

More information about automatic generation of documentation can be found 
[here](https://fastapi.tiangolo.com/tutorial/first-steps/) and
[here](https://fastapi.tiangolo.com/tutorial/metadata/)


## Contribution

### Invoke tasks

- `build` builds the application docker image (optional args `--no-cache`)
- `clear-cache` removes python cache files and directories
- `coverage` checks the test coverage
- `image-rm` removes the app's docker image
- `lint` run flake8 (optional args `--count=True/False`, `--hide=True/False`)
- `make-migration` prepare db migration file; args `--name=migration_name` 
(optional args `--autogenerate=True/False`)
- `prepare-docker-network` creates a network for docker containers
- `prepare-postgres-container` creates a container with a postgres instance
- `prepare-redis-container` creates a container with a redis instance
- `pull-request` alias `pr` checks the application before pull request (style, tests, etc.)
- `report` prepares a test report (optional args `--show_missing`)
- `style` generates a text report from data gathered by the `coverage` command
- `test` run pytest
- `update-database` applies the migrations to the database
