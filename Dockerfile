FROM python:3.9

ENV PYTHONUNBUFFERED 1
ENV PYTHONPATH /opt
ENV POETRY_VIRTUALENVS_CREATE=false

RUN apt-get update && apt-get install -y build-essential python3-dev libldap2-dev libsasl2-dev

RUN mkdir /opt/app
WORKDIR /opt/

COPY ./.flake8 /opt/.flake8
COPY ./pyproject.toml /opt/pyproject.toml
COPY ./poetry.lock /opt/poetry.lock
COPY ./alembic.ini /opt/alembic.ini
COPY ./uvicorn_logging.ini /opt/uvicorn_logging.ini
COPY ./app/ /opt/app

RUN pip3 install pip==20.1.1 poetry

RUN poetry install --no-interaction
RUN awsv2 --install
