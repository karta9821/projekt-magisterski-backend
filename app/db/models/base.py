from typing import Any

from sqlalchemy import Column, DateTime
from sqlalchemy.ext.declarative import as_declarative, declared_attr
from sqlalchemy.orm import class_mapper
from sqlalchemy.sql import func


@as_declarative()
class Base:
    id: Any
    __name__: str

    created_at = Column(DateTime(timezone=True), server_default=func.now())

    @declared_attr
    def __tablename__(cls) -> str:
        return cls.__name__.lower()

    def asdict(self):
        return dict(
            (col.name, getattr(self, col.name))
            for col in class_mapper(self.__class__).mapped_table.c
        )
