from app.db.models.base import Base
from sqlalchemy import Column, Integer, String


class BaseNewspaper(Base):
    __abstract__ = True

    id = Column(Integer, primary_key=True, index=True)
    filename = Column(String(126), nullable=False)
    s3_path = Column(String(126), nullable=False)


class RawNewspaper(BaseNewspaper):
    __tablename__ = "raw_newspaper"
