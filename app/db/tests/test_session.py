from unittest import TestCase
from unittest.mock import call, patch

from app.db.session import db_session


class SessionTestCase(TestCase):
    @patch("app.db.session.settings")
    @patch("app.db.session.create_engine")
    @patch("app.db.session.scoped_session")
    @patch("app.db.session.sessionmaker")
    def test_db_session(
        self,
        mock_sessionmaker,
        mock_scoped_session,
        mock_create_engine,
        mock_settings,
    ):
        with db_session():
            mock_scoped_session.assert_called_once_with(
                mock_sessionmaker.return_value,
            )

        mock_create_engine.assert_called_once_with(
            mock_settings.SQLALCHEMY_DATABASE_URI,
            pool_pre_ping=True,
        )
        mock_sessionmaker.assert_called_once_with(
            autocommit=False,
            autoflush=True,
            bind=mock_create_engine.return_value,
        )
        self.assertEqual(
            mock_scoped_session.mock_calls,
            [call(mock_sessionmaker.return_value), call().close()],
        )
        mock_settings.assert_not_called()
