from typing import Any
from unittest import TestCase

from app.db.models.base import Base


class BaseTestCase(TestCase):
    def test___annotations__(self):
        self.assertEqual(Base.__annotations__, {"id": Any, "__name__": str})

    def test___tablename__(self):
        self.assertEqual(Base.__tablename__, "base")
