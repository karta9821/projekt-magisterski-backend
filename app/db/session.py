from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

from app.configuration.settings import settings


@contextmanager
def db_session():
    engine = create_engine(
        settings.SQLALCHEMY_DATABASE_URI, pool_pre_ping=True
    )
    db_session = scoped_session(
        sessionmaker(autocommit=False, autoflush=True, bind=engine)
    )
    yield db_session
    db_session.close()
