from aioredis import from_url

from app.configuration.settings import settings

# this should probably be initiated in the container
# and distributed via dependency injection
# https://python-dependency-injector.ets-labs.org/examples/fastapi-redis.html
SessionLocal = from_url(
    f"redis://{settings.REDIS_HOST}:{settings.REDIS_PORT}",
    password=settings.REDIS_PASSWORD,
)
