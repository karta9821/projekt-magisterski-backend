from typing import Generator

import pytest
from fastapi.testclient import TestClient

from app.cache.redis import SessionLocal as RedisSessionLocal
from app.configuration.settings import settings
from app.main import app


def get_test_client() -> TestClient:
    settings.SENTRY_DSN = None
    return TestClient(app)


@pytest.fixture(scope="module")
def client() -> Generator:
    with get_test_client() as c:
        yield c


@pytest.fixture(scope="session")
def redis() -> Generator:
    yield RedisSessionLocal()
