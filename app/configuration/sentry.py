import logging
from typing import List

import sentry_sdk
from sentry_sdk.integrations.logging import LoggingIntegration

from app.configuration.settings import settings


def get_integrations() -> List:
    integrations = []
    if settings.SENTRY_LOGGING:
        integrations.append(
            LoggingIntegration(
                level=logging.getLevelName(settings.SENTRY_LOGGING_LEVEL),
                event_level=logging.getLevelName(
                    settings.SENTRY_LOGGING_EVENT_LEVEL
                ),
            )
        )
    return integrations


def initialise_sentry():
    sentry_sdk.init(
        dsn=settings.SENTRY_DSN,
        traces_sample_rate=settings.SENTRY_TRACES_SAMPLE_RATE,
        environment=settings.SENTRY_ENVIRONMENT,
        integrations=get_integrations(),
    )
