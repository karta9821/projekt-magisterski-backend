from asgi_correlation_id.log_filters import correlation_id_filter
from pydantic import BaseModel

from app.configuration.settings import settings


class LogConfig(BaseModel):
    LOGGER_NAME: str = "root"
    LOG_FORMAT: str = (
        "%(asctime)s %(levelname)s | %(correlation_id)s | "
        "%(name)s | %(message)s'"
    )
    LOG_LEVEL: str = settings.LOGGER_LEVEL
    LOG_FILE_NAME: str = f"{settings.LOGGER_FILE_PATH}app.log"

    version = 1
    disable_existing_loggers = False
    formatters = {
        "default": {
            "()": "uvicorn.logging.DefaultFormatter",
            "fmt": LOG_FORMAT,
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
    }
    filters = {
        "correlation_id": {"()": correlation_id_filter()},
    }
    handlers = {
        "console_handler": {
            "formatter": "default",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",
            "filters": ["correlation_id"],
        },
        "file_handler": {
            "formatter": "default",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": LOG_FILE_NAME,
            "mode": "a",
            "maxBytes": 10000,
            "backupCount": 10,
            "filters": ["correlation_id"],
        },
    }
    loggers = {
        "root": {
            "handlers": ["console_handler", "file_handler"],
            "level": LOG_LEVEL,
        },
    }
