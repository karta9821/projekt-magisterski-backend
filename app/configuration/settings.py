import secrets
from typing import Any, Dict, List, Optional, Union

from pydantic import AnyHttpUrl, BaseSettings, HttpUrl, PostgresDsn, validator


class Settings(BaseSettings):
    API_V1_STR: str = "/api/v1"
    SECRET_KEY: str = secrets.token_urlsafe(32)
    # BACKEND_CORS_ORIGINS is a JSON-formatted list of origins
    # e.g: ["http://localhost", "http://localhost:3000",
    #       "http://local.dockertoolbox.tiangolo.com"]
    BACKEND_CORS_ORIGINS: List[AnyHttpUrl] = []

    @validator("BACKEND_CORS_ORIGINS", pre=True)
    def assemble_cors_origins(
        cls, v: Union[str, List[str]]
    ) -> Union[List[str], str]:
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v)

    PROJECT_NAME: str

    SENTRY_DSN: Union[HttpUrl, bool] = False
    SENTRY_TRACES_SAMPLE_RATE: Optional[float]
    SENTRY_ENVIRONMENT: Optional[str]
    SENTRY_LOGGING: bool = False
    SENTRY_LOGGING_LEVEL: str = (
        "INFO"  # Capture logs from a given level and add to the event
    )
    SENTRY_LOGGING_EVENT_LEVEL: str = (
        "ERROR"  # Captured logs from the given level are sent as events
    )

    @validator("SENTRY_DSN", pre=True)
    def sentry_dsn_can_be_blank(cls, v: str) -> Union[str, bool]:
        if not v:
            return False
        return v

    @validator("SENTRY_LOGGING", pre=True)
    def sentry_logging_can_be_blank(cls, v: str) -> Optional[bool]:
        return bool(v)

    # RequestID
    REQUEST_ID_HEADER_NAME: str
    REQUEST_ID_VALIDATION: bool

    # DATABASE
    POSTGRES_SERVER: str
    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_DB: str
    SQLALCHEMY_DATABASE_URI: Optional[PostgresDsn] = None

    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(
        cls, v: Optional[str], values: Dict[str, Any]
    ) -> Any:
        if isinstance(v, str):
            return v
        return PostgresDsn.build(
            scheme="postgresql",
            user=values.get("POSTGRES_USER"),
            password=values.get("POSTGRES_PASSWORD"),
            host=values.get("POSTGRES_SERVER"),
            path=f"/{values.get('POSTGRES_DB') or ''}",
        )

    # CACHE
    REDIS_HOST: str
    REDIS_PORT: str
    REDIS_PASSWORD: str

    # LOGGER
    LOGGER_LEVEL: str
    LOGGER_FILE_PATH: str

    # Minio
    MINIO_ROOT_USER: Optional[str]
    MINIO_ROOT_PASSWORD: Optional[str]

    # AWS
    AWS_ACCESS_KEY_ID: str
    AWS_SECRET_ACCESS_KEY: str
    AWS_ENDPOINT_URL: str
    AWS_S3_UPLOAD_BUCKET: str
    AWS_S3_DOWNLOAD_BUCKET: str

    class Config:
        env_file = ".env"
        case_sensitive = True


settings = Settings()
