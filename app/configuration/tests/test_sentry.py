from unittest import TestCase
from unittest.mock import Mock, patch

from app.configuration.sentry import (
    get_integrations,
    initialise_sentry,
    sentry_sdk,
)


class SentryTestCase(TestCase):
    @patch("app.configuration.sentry.settings")
    @patch("app.configuration.sentry.logging")
    @patch("app.configuration.sentry.LoggingIntegration")
    def test_get_integrations_if_enabled_logging(
        self, mock_LoggingIntegration, mock_logging, mock_settings
    ):
        mock_settings.SENTRY_LOGGING = True
        get_integrations()
        mock_LoggingIntegration.assert_called_once_with(
            level=mock_logging.getLevelName(),
            event_level=mock_logging.getLevelName(),
        )

    @patch("app.configuration.sentry.settings")
    @patch("app.configuration.sentry.LoggingIntegration")
    def test_get_integrations_if_disabled_logging(
        self, mock_LoggingIntegration, mock_settings
    ):
        mock_settings.SENTRY_LOGGING = False
        get_integrations()
        mock_LoggingIntegration.assert_not_called()

    @patch.object(sentry_sdk, "init", Mock(return_value=None))
    @patch("app.configuration.sentry.get_integrations")
    @patch("app.configuration.sentry.settings")
    def test_initialise_sentry(self, mock_settings, mock_get_integrations):
        initialise_sentry()
        sentry_sdk.init.assert_called_once_with(
            dsn=mock_settings.SENTRY_DSN,
            traces_sample_rate=mock_settings.SENTRY_TRACES_SAMPLE_RATE,
            environment=mock_settings.SENTRY_ENVIRONMENT,
            integrations=mock_get_integrations(),
        )
