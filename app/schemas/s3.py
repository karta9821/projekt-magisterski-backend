from pydantic import BaseModel, Extra, Field


class S3UploadResponse(BaseModel):
    version: str = Field(alias="VersionId")

    class Config:
        extra = Extra.ignore
