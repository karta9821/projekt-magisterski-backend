from celery import Celery
from celery.app.log import TaskFormatter
from celery.signals import after_setup_logger, after_setup_task_logger

from app.configuration.settings import settings


def get_redis_url():
    user_and_password = f":{settings.REDIS_PASSWORD}"
    host_and_port = f"{settings.REDIS_HOST}:{settings.REDIS_PORT}"
    database = "0"

    return f"redis://{user_and_password}@{host_and_port}/{database}"


celery = Celery(__name__)
celery.conf.broker_url = get_redis_url()
celery.conf.result_backend = get_redis_url()
celery.conf.task_default_queue = settings.PROJECT_NAME


@after_setup_logger.connect
@after_setup_task_logger.connect
def setup_task_logger(logger, *args, **kwargs):
    for handler in logger.handlers:
        handler.setFormatter(
            TaskFormatter(
                " ".join(
                    [
                        "%(asctime)s %(levelname)s |",
                        "%(task_id)s - %(task_name)s - %(name)s |",
                        "%(message)s",
                    ]
                )
            )
        )
