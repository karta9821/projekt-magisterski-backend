from unittest import TestCase
from unittest.mock import Mock, patch

from app.tasks.celery import get_redis_url, setup_task_logger


class CeleryTestCase(TestCase):
    @patch("app.tasks.celery.settings")
    def test_get_redis_url(self, mock_settings):
        mock_settings.REDIS_PASSWORD = "password"
        mock_settings.REDIS_HOST = "host"
        mock_settings.REDIS_PORT = "123"
        self.assertEqual(get_redis_url(), "redis://:password@host:123/0")

    @patch("app.tasks.celery.TaskFormatter")
    def test_setup_task_logger(self, mock_TaskFormatter):
        set_formatter_mock = Mock()
        handler_mock = Mock(setFormatter=set_formatter_mock)
        logger_mock = Mock(handlers=[handler_mock])

        setup_task_logger(logger_mock)

        set_formatter_mock.assert_called_once_with(
            mock_TaskFormatter.return_value
        )
        mock_TaskFormatter.assert_called_once_with(
            " ".join(
                [
                    "%(asctime)s %(levelname)s |",
                    "%(task_id)s - %(task_name)s - %(name)s |",
                    "%(message)s",
                ]
            )
        )
