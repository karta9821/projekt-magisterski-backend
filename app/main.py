from logging.config import dictConfig

from asgi_correlation_id import CorrelationIdMiddleware
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from app.api.router import router
from app.configuration.logging_config import LogConfig
from app.configuration.sentry import initialise_sentry
from app.configuration.settings import settings

dictConfig(LogConfig().dict())


app = FastAPI(
    title=settings.PROJECT_NAME,
    openapi_url=f"{settings.API_V1_STR}/openapi.json",
)

app.add_middleware(
    CorrelationIdMiddleware,
    header_name=settings.REQUEST_ID_HEADER_NAME,
    validate_header_as_uuid=settings.REQUEST_ID_VALIDATION,
)

if settings.SENTRY_DSN:
    initialise_sentry()

if settings.BACKEND_CORS_ORIGINS:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[
            str(origin) for origin in settings.BACKEND_CORS_ORIGINS
        ],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

app.include_router(router, prefix=settings.API_V1_STR)
