from typing import Optional

import boto3

from app.configuration.settings import settings
from app.schemas.s3 import S3UploadResponse


class AWSS3:
    def __init__(
        self,
        upload_bucket: Optional[str] = None,
        download_bucket: Optional[str] = None,
    ):
        self.client = boto3.client(
            service_name="s3",
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
            endpoint_url=settings.AWS_ENDPOINT_URL,
        )
        self.upload_bucket = upload_bucket or settings.AWS_S3_UPLOAD_BUCKET
        self.download_bucket = (
            download_bucket or settings.AWS_S3_DOWNLOAD_BUCKET
        )

    def put_object(self, name: str, file: bytes) -> S3UploadResponse:
        response = self.client.put_object(
            Key=name,
            Body=file,
            Bucket=self.upload_bucket,
        )
        return S3UploadResponse(VersionId=response.get("VersionId", "1"))
