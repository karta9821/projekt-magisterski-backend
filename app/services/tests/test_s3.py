import pytest

from app.services.s3 import AWSS3


class TestAWSS3:
    @pytest.fixture(autouse=True)
    def setup(self, mocker):
        self.mock_settings = mocker.patch("app.services.s3.settings")
        self.mock_client = mocker.patch("app.services.s3.boto3.client")

    @pytest.mark.parametrize(
        "input_, expected",
        [
            (
                {"upload_bucket": None, "download_bucket": None},
                {"upload_bucket": None, "download_bucket": None},
            ),
            (
                {
                    "upload_bucket": "some-upload-bucket",
                    "download_bucket": "some-download-bucket",
                },
                {
                    "upload_bucket": "some-upload-bucket",
                    "download_bucket": "some-download-bucket",
                },
            ),
        ],
    )
    def test__init__(self, input_, expected):
        # given
        upload_bucket = (
            expected["upload_bucket"]
            or self.mock_settings.AWS_S3_UPLOAD_BUCKET
        )
        download_bucket = (
            expected["download_bucket"]
            or self.mock_settings.AWS_S3_DOWNLOAD_BUCKET
        )

        # when
        s3_service = AWSS3(**input_)

        # then
        self.mock_client.assert_called_once_with(
            service_name="s3",
            aws_access_key_id=self.mock_settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=self.mock_settings.AWS_SECRET_ACCESS_KEY,
            endpoint_url=self.mock_settings.AWS_ENDPOINT_URL,
        )
        assert s3_service.upload_bucket == upload_bucket
        assert s3_service.download_bucket == download_bucket

    def test_put_object(self, mocker):
        # given
        mock_s3_upload_response = mocker.patch(
            "app.services.s3.S3UploadResponse"
        )
        name = "test-file"
        file = b"some-file"

        # when
        s3_service = AWSS3()
        result = s3_service.put_object(name=name, file=file)

        # then
        self.mock_client.return_value.put_object.assert_called_once_with(
            Key=name,
            Body=file,
            Bucket=self.mock_settings.AWS_S3_UPLOAD_BUCKET,
        )
        self.mock_client.return_value.put_object.return_value.get.assert_called_once_with(
            "VersionId", "1"
        )
        mock_s3_upload_response.assert_called_once_with(
            VersionId=self.mock_client.return_value.put_object.return_value.get.return_value
        )
        assert result == mock_s3_upload_response.return_value
